# Drive Thru App (RoboMacher)

### Python 2.7

This project was done as a part of Tel-Aviv University CIM lab class


### Video

https://www.youtube.com/watch?v=LjGUbdqd7t8&feature=youtu.be

### Summary

- Development of a drive-thru interface for a doughnut restaurant.
- Interface includes an Android app for orders input and notifications, and a SCORBASE (Robotic control software) program for change calculation and two-way physical payment handout, using image recognition.
- The Android app is written in Python and communicates with a MySQL database.

### Files

- **main.py** - App backend
- **Loop.py** - Get external input
- **kivy.kv** - Kivy CSS
- **SCORBASE files** - Robotic control arm commands 
- **VBS files** - Reading/Writing to external files, for communication between Android app and SCORBASE programs