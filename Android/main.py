__author__ = 'Student'
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.core.audio import SoundLoader
import mysql.connector as mdb,sys
from kivy.clock import Clock
import time

class InputForm(BoxLayout):
    #Define objects for amounts and prices
    orders = ObjectProperty()
    bamba_amount = ObjectProperty()
    chocolate_amount=ObjectProperty()
    baileys_amount=ObjectProperty()
    timtam_amount=ObjectProperty()
    surprise_amount=ObjectProperty()
    bamba_price = ObjectProperty()
    chocolate_price=ObjectProperty()
    baileys_price=ObjectProperty()
    timtam_price=ObjectProperty()
    surprise_price=ObjectProperty()

    #IP for registartion
    ip = ObjectProperty()

    #Product name list
    Products = ["Bamba-Takila","Chocolate-Vodka","Baileys","Tim-Tam Rum","Surprise"]
    #Prices list
    Prices=[20,20,30,30,50]

    def connect(self):
        'Connects to MySQL server'
        #Building new DB and new table if not exists already


        #Define strings of prices (For KV file)
        self.bamba_price.text=str(self.Prices[0]) + " $"
        self.chocolate_price.text=str(self.Prices[1]) + " $"
        self.baileys_price.text=str(self.Prices[2]) + " $"
        self.timtam_price.text=str(self.Prices[3]) + " $"
        self.surprise_price.text=str(self.Prices[4]) + " $"

        #COnnect to MySql server
        con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab')
        cur = con.cursor()
        #Create new database if do not exist
        cur.execute('CREATE DATABASE if not exists DrunkinDonuts;')
        con.commit()  # Makes transaction
        con.close()  # Close transaction
        # Connects get the ip address from user
        con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
        cur = con.cursor()
        #Create table for orders, products and prices

        cur.execute("CREATE TABLE IF NOT EXISTS Orders \
                    (Order_number INTEGER(11) NOT NULL, Product_name varchar(40) NOT NULL, Ordered_amount INTEGER(11) NOT NULL, Order_status \
                    varchar(20) NOT NULL)")
        '''
        cur.execute("CREATE TABLE IF NOT EXISTS Ready (Order_number INTEGER(11) PRIMARY KEY, Price INTEGER(11), Picked VARCHAR(20))")
        '''
        #Assing current order number- find highest in table and add 1 to it
        order_query= ("SELECT max(Order_number) FROM Orders")
        cur.execute(order_query)

        order_numbers = [item[0] for item in cur.fetchall()] #Quering database

        order_number=order_numbers[0]+1

        self.order_number=order_number

        #Create prices table - should not be in use
        '''
        cur.execute("CREATE TABLE IF NOT EXISTS Prices (Product_name varchar(40) NOT NULL, Price INTEGER(10) NOT NULL)")
        for i in range(len(self.Products)):
            cur.execute("INSERT INTO Prices (Product_name, Price) VALUES ('%s',%d)"
                        % (self.Products[i],(self.Prices[i])))
        '''
        #Close connection
        con.commit()
        con.close()

    def PlaceOrder(self):
        'This page is accessed after filling amounts to be ordered'

        #Connect to MySQL server
        con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
        cur = con.cursor()  # Making cursor

        #Insert ordered amounts into orders table
        if int(self.bamba_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Bamba-Takila", int(self.bamba_amount.text),'Open'))

        if int(self.chocolate_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Chocolate-Vodka", int(self.chocolate_amount.text),'Open'))
        if int(self.baileys_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Baileys", int(self.baileys_amount.text),'Open'))
        if int(self.timtam_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Tim-Tam Rum", int(self.timtam_amount.text),'Open'))
        if int(self.surprise_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Surprise", int(self.surprise_amount.text),'Open'))
        con.commit()

        #Select all lines of current order
        query=("SELECT Orders.Product_name, Ordered_amount,Price*Ordered_amount From Orders Left Join Prices on Orders.Product_name = Prices.Product_name WHERE Order_Number= %d" %self.order_number)
        cur.execute(query) #Run query
        rows=cur.fetchall() #Retrive all rows from query.
        self.clear_widgets()# Clears InputForm
        #Go to Confirmation page
        self.add_widget(ConfirmOrder(self.ip,self.order_number,rows))# Adds ShowRecords as new form
        con.commit()
        con.close()

#Confirmation page
class ConfirmOrder(BoxLayout):
    order=ObjectProperty()#Order number
    products=ObjectProperty()# Products
    amounts=ObjectProperty()#AMounts of order
    totalprice=ObjectProperty()#Total price per line
    finalprice=ObjectProperty()#Total price for order


    def __init__(self,ip,order_number,rows):
        self.order_number = order_number
        self.ip=ip
        self.rows=rows
        super(ConfirmOrder,self).__init__() #Call builder of BoxLayout because we don't use default builder
        self.show(rows,order_number) #Call method to show results,

    def show(self,rows,order_number):
        #If noting was ordered
        print rows
        if not rows:
            self.order.text='No Products were chosen'

        #Pull data from query to text strings
        elif rows:
            self.order.text=str(order_number)

            self.products.text="Product" + '\n'
            self.amounts.text="Amount" + '\n'
            self.totalprice.text="Price" + '\n'
            price_calculator=0
            print rows
            for r in rows:
                if len(r)>0:
                    self.products.text+=str(r[0])+'\n'

            for r in rows:
                if len(r)>0:
                    self.amounts.text+=str(r[1])+'\n'

            for r in rows:
                if len(r)>0:
                    self.totalprice.text+=str(r[2])+'\n'
                    price_calculator += int(r[2])

            #Calculate final price
            self.finalprice.text = str(price_calculator) + " $"

    def confirm(self):
        #If nothing was ordered- go back to home page
        if self.rows==[]:
            self.clear_widgets()  # Clears InputForm
            self.add_widget(ReInputForm(self.order_number,self.ip))# Adds ShowRecords as new form
        else:
            #GO to status page
            status='Please hold - Your order is being prepared'
            self.clear_widgets()  # Clears InputForm
            self.add_widget(Waitforsignal(self.order_number,self.ip,status))# Adds ShowRecords as new form

    def back(self):
        #Back button- deletes order and goes back to home page
        con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
        cur = con.cursor()

        delete_query= ("DELETE FROM Orders WHERE Order_number=%d" %self.order_number)
        cur.execute(delete_query)
        con.commit()
        con.close()
        self.clear_widgets()
        self.add_widget(ReInputForm(self.order_number,self.ip))# Adds ShowRecords as new form


class ReInputForm(BoxLayout):
    #Go back to homepage without filling in IP again
    orders = ObjectProperty()
    bamba_amount = ObjectProperty()
    chocolate_amount=ObjectProperty()
    baileys_amount=ObjectProperty()
    timtam_amount=ObjectProperty()
    surprise_amount=ObjectProperty()

    bamba_price = ObjectProperty()
    chocolate_price=ObjectProperty()
    baileys_price=ObjectProperty()
    timtam_price=ObjectProperty()
    surprise_price=ObjectProperty()

    #IP for registartion
    ip = ObjectProperty()


    ip = ObjectProperty()
    Products = ["Bamba-Takila Doughnut","Chocolate-Vodka Doughnut","baileys Doughnut","Tim-Tam Rum Doughnut","Surprise Doughnut"]
    Prices=[20,20,30,30,50]
    def __init__(self, order_number,ip):

        self.order_number=order_number
        self.ip=ip
        super(ReInputForm,self).__init__()

        self.bamba_price.text=str(self.Prices[0]) + " $"
        self.chocolate_price.text=str(self.Prices[1]) + " $"
        self.baileys_price.text=str(self.Prices[2]) + " $"
        self.timtam_price.text=str(self.Prices[3]) + " $"
        self.surprise_price.text=str(self.Prices[4]) + " $"



    def RePlaceOrder(self):
        'Go Back to confimration page Order number remains the same'

        #COnnect to MySQL server
        con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
        cur = con.cursor()  # Making cursor
        #Insert data
        if int(self.bamba_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Bamba-Takila", int(self.bamba_amount.text),'Open'))

        if int(self.chocolate_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Chocolate-Vodka", int(self.chocolate_amount.text),'Open'))
        if int(self.baileys_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Baileys", int(self.baileys_amount.text),'Open'))
        if int(self.timtam_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Tim-Tam Rum", int(self.timtam_amount.text),'Open'))
        if int(self.surprise_amount.text)>0:# Check input
            cur.execute("INSERT INTO Orders (Order_number,Product_name,Ordered_amount,Order_status) VALUES (%d,'%s',%d,'%s')"
                        % (self.order_number,"Surprise", int(self.surprise_amount.text),'Open'))
        con.commit()

        query=("SELECT Orders.Product_name, Ordered_amount,Price*Ordered_amount From Orders Left Join Prices on Orders.Product_name = Prices.Product_name WHERE Order_Number= %d" %self.order_number)
        cur.execute(query) #Quering database
        rows=cur.fetchall() #Retrive all rows from query.
        self.clear_widgets()  # Clears InputForm
        self.add_widget(ConfirmOrder(self.ip,self.order_number, rows))# Adds ShowRecords as new form
        con.commit()
        con.close()


class Waitforsignal(BoxLayout):

    #Status page - after order was received and waiting for preaparation
    counter_pending=0
    counter_ready=0
    counter_picked=0
    ordermessage=ObjectProperty()
    ordernumber=ObjectProperty()
    def __init__(self,order_number,ip,status):

        self.order_number=order_number
        self.ip=ip


        super(Waitforsignal,self).__init__() #Call builder of BoxLayout because we don't use default builder
        self.ordernumber.text= 'Order # ' + str(order_number)
        self.ordermessage.text= str(status)

        Clock.schedule_interval(self.pending, 3)

    def pending(self, dt):
        con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
        cur = con.cursor()
        query=("SELECT Order_status From Orders  WHERE Order_Number= %d" %int(self.order_number))
        cur.execute(query)
        con.close()
        cur_status = [item[0] for item in cur.fetchall()] #Quering database
        print str(cur_status[0])
        if str(cur_status[0])=='Open':


            con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
            cur = con.cursor()

            cur.execute("UPDATE Orders SET Order_status='Pending' WHERE Order_number=%d AND Order_status='Open'" %(self.order_number))
            con.commit()
            con.close()
        if str(cur_status[0])=='Pending':
            con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
            cur = con.cursor()
            self.order_number=47
            cur.execute("SELECT Order_number,Price FROM Ready WHERE Order_number=%d AND Picked='No'" %(self.order_number))
            ready_check=cur.fetchall()
            con.close()
            print ready_check
            if ready_check !=[]:
                ready_order=int(ready_check[0][0])
            else:
                ready_order=0
            if ready_order==self.order_number:

                con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
                cur = con.cursor()

                query2=("UPDATE Orders SET Order_status='Ready' WHERE Order_number=%d" %int(self.order_number))

                cur.execute(query2)
                con.commit()

                con.close()
                status="Order is ready- Please pick up"
                self.clear_widgets()  # Clears InputForm
                self.add_widget(Waitforsignal(self.order_number,self.ip,status))# Adds ShowRecords as new form


        if str(cur_status[0]) =='Ready':
            if self.counter_ready==0:

                fname="alert.wav"
                sound=SoundLoader.load(fname)
                sound.play()
                self.counter_ready =1
            cur.execute("SELECT Order_number, Picked FROM Ready WHERE Order_number=%d AND Picked='Yes'" %(self.order_number))
            pick_check=cur.fetchall()
            con.close()
            print pick_check
            if pick_check !=[]:
                picked_order=int(pick_check[0][0])
            else:
                picked_order=0
            print picked_order
            print "picked " + str(picked_order)
            print "order " + str(self.order_number)
            if picked_order==self.order_number:

                con = mdb.connect(host=self.ip.text, user='cimlab', passwd='CimLab', db='DrunkinDonuts')
                cur = con.cursor()

                query2=("UPDATE Orders SET Order_status='Picked' WHERE Order_number=%d" %(self.order_number))

                cur.execute(query2)
                con.commit()
                con.close()
                status='Thank you!'
                self.clear_widgets()  # Clears InputForm
                self.add_widget(Waitforsignal(self.order_number,self.ip,status))# Adds ShowRecords as new form


        if str(cur_status[0]) =='Picked':
            if self.counter_picked==0:

                fname="thankyou.wav"
                sound=SoundLoader.load(fname)
                sound.play()
                self.counter_picked=1
            else:
                sys.exit()



class KivyApp(App):
    pass


if __name__ == '__main__':
    KivyApp().run()